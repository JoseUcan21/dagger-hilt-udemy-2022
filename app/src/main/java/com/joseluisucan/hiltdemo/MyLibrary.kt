package com.joseluisucan.hiltdemo

import android.util.Log
import javax.inject.Inject

interface MyLibrary{
    fun create():MyLibrary
    fun sayHello()
}

class MyLibraryImp @Inject constructor():MyLibrary{
    override fun create(): MyLibrary {
        Log.d("Mensaje","Simular librería")
        return this
    }

    override fun sayHello() {
        Log.d("Mensaje","Hola desde Librería externa")
    }

}