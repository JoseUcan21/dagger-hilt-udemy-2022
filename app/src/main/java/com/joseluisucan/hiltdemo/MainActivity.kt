package com.joseluisucan.hiltdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    @Inject lateinit var repository: Repository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        repository.guardarNombre("Benito")

        val txtName = findViewById<TextView>(R.id.txtName)
        txtName.setOnClickListener {
            txtName.text = repository.obtenerNombre()
        }
    }
}