package com.joseluisucan.hiltdemo

import javax.inject.Inject

class Repository @Inject constructor(
    private val preferencias: Preferencias,
    private val libreria:MyLibrary
    ) {

    fun guardarNombre(nombre:String){
        preferencias.guardarNombre(nombre)
    }

    fun obtenerNombre():String{
        libreria.sayHello()
        return preferencias.obtenerNombre()
    }
}