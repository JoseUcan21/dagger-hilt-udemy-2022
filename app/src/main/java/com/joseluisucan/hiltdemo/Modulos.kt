package com.joseluisucan.hiltdemo

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object Modulos {

    @Provides
    @Singleton
    fun provideLibrary():MyLibrary{
        return MyLibraryImp().create()
    }

}