package com.joseluisucan.hiltdemo

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class Preferencias @Inject constructor(@ApplicationContext context: Context) {
    val KEY_FILE = "KEY_FILE"
    val KEY_NAME = "KEY_NAME"

    val sharedPref = context.getSharedPreferences(KEY_FILE, Context.MODE_PRIVATE)

    fun guardarNombre(nombre:String){
        with(sharedPref.edit()){
            putString(KEY_NAME,nombre)
            apply()
        }
    }

    fun obtenerNombre() = sharedPref.getString(KEY_NAME,"")!!
}